<?php
class Articulo extends CI_Model
 {
   function __construct()
   {
     parent::__construct();
   }
   //Insertar nuevos hospitales
   function insertar($datos){
     $respuesta=$this->db->insert("articulo",$datos);
     return $respuesta;
   }
   //Consulta de datos
   function consultarTodos(){
     $this->db->select('articulo.*, revista.nombre as nombre_revista');
     $this->db->join('revista', 'revista.id = articulo.id_revista');
     $articulos=$this->db->get("articulo");
     if ($articulos->num_rows()>0) {
       return $articulos->result();
     } else {
       return false;
     }
   }
   //eliminacion de hospital por id
   function eliminar($id){
       $this->db->where("id",$id);
       return $this->db->delete("articulo");
   }
   //consulta de un solo hospital
   function obtenerPorId($id){
      $this->db->where("id",$id);
      $autor=$this->db->get("articulo");
      if ($autor->num_rows()>0) {
        return $autor->row();
      } else {
        return false;
      }
    }
    // Actualizar una revista por su ID
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("articulo", $datos);
    }

  }
