<?php
class Autor extends CI_Model
 {
   function __construct()
   {
     parent::__construct();
   }
   //Insertar nuevos hospitales
   function insertar($datos){
     $respuesta=$this->db->insert("autor",$datos);
     return $respuesta;
   }
   //Consulta de datos
   function consultarTodos(){
     $autores=$this->db->get("autor");
     if ($autores->num_rows()>0) {
       return $autores->result();
     } else {
       return false;
     }
   }
   //eliminacion de hospital por id
   function eliminar($id){
       $this->db->where("id",$id);
       return $this->db->delete("autor");
   }
   //consulta de un solo hospital
   function obtenerPorId($id){
      $this->db->where("id",$id);
      $autor=$this->db->get("autor");
      if ($autor->num_rows()>0) {
        return $autor->row();
      } else {
        return false;
      }
    }
    // funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db
                  ->update("autor",$datos);
    }




 }//Fin de la clase
?>
