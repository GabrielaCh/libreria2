<?php
/* CI_Model es la clase padre ya viene definido en Codein */
// Crear el modelo -> Se debe crear en singular
class Seccion extends CI_Model
{

    function __construct()
    {
        // Constructor de la clase padre
        parent::__construct();
    }// Fin de la función

    // Nueva función para insertar nuevos
    function insertar($datos)
    {
        // Crear variable
        $respuesta = $this->db->insert("secciones", $datos); // insert->permite ingresar registros
        return $respuesta;
    }

    // Consulta de datos
    function consultarTodos()
    {
        //
        $secciones = $this->db->get("secciones");

        if ($secciones->num_rows() > 0) {

            return $secciones->result();
        } else {
            return false;
        }
    }// Fin función consultarTodos

    // Eliminación por id
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("secciones");
    }

    // Consulta de uno solo
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $seccion = $this->db->get("secciones");
        if ($seccion->num_rows() > 0) {
            return $seccion->row();
        } else {
            return false;
        }
    }

    // Función para Actualizar
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("secciones", $datos);
    }
}// Fin de la clase
?>
