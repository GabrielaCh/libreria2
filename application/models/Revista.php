<?php
class Revista extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar una nueva revista
    function insertar($datos)
    {
        $respuesta = $this->db->insert("revista", $datos);
        return $respuesta;
    }
    //Consultar nombres e IDs de todas las revistas
    public function consultarNombresRevistas()
    {
      // Consulta los nombres y los IDs de las revistas desde la base de datos
      $this->db->select('id, nombre');
      $query = $this->db->get('revista');
      // Retorna un array de nombres e IDs de revistas
      return $query->result_array();
    }



    // Consultar todas las revistas
    function consultarTodos()
    {
        $revistas = $this->db->get("revista");
        if ($revistas->num_rows() > 0) {
            return $revistas->result();
        } else {
            return false;
        }
    }

    // Eliminar una revista por su ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("revista");
    }

    // Obtener una revista por su ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $revista = $this->db->get("revista");
        if ($revista->num_rows() > 0) {
            return $revista->row();
        } else {
            return false;
        }
    }

    // Actualizar una revista por su ID
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("revista", $datos);
    }
}
?>
