<?php
class Indexacion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar nueva indexación en la base de datos
    function insertar($datos)
    {
        $respuesta = $this->db->insert("indexacion", $datos);
        return $respuesta;
    }

    // Consultar todas las indexaciones
    function consultarTodos()
    {
        $indexaciones = $this->db->get("indexacion");
        if ($indexaciones->num_rows() > 0) {
            return $indexaciones->result();
        } else {
            return false;
        }
    }

    // Eliminar una indexación por su ID
    function eliminar($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete("indexacion");
    }

    // Obtener una indexación por su ID
    function obtenerPorId($id)
    {
        $this->db->where("id", $id);
        $indexacion = $this->db->get("indexacion");
        if ($indexacion->num_rows() > 0) {
            return $indexacion->row();
        } else {
            return false;
        }
    }

    // Actualizar una indexación en la base de datos
    function actualizar($id, $datos)
    {
        $this->db->where("id", $id);
        return $this->db->update("indexacion", $datos);
    }
}
?>
