<?php


class NombreDelModelo extends CI_Model {
    // Método para obtener los nombres de los autores
    public function obtenerNombresAutores() {
        // Realizar consulta para obtener nombres de autores
        $query = $this->db->select('nombre')->get('autor');
        return $query->result();
    }

    // Método para obtener los nombres de las revistas
    public function obtenerNombresRevistas() {
        // Realizar consulta para obtener nombres de revistas
        $query = $this->db->select('nombre')->get('revista');
        return $query->result();
    }

    // Método para obtener las fotos de indexación
    public function obtenerFotosIndexacion() {
        // Realizar consulta para obtener fotos de indexación
        $query = $this->db->select('firma')->get('indexacion');
        return $query->result();
    }

}
?>
