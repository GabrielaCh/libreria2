<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'titulo' => array(
        array(
            'field' => 'titulo',
            'label' => 'Título',
            'rules' => 'required|regex_match[/^[A-Za-z\s]+$/]',
            'errors' => array(
                'required' => 'El campo %s es obligatorio.',
                'regex_match' => 'El campo %s solo puede contener letras y espacios.'
            )
        )
    ),
    'resumen' => array(
        array(
            'field' => 'resumen',
            'label' => 'Resumen',
            'rules' => 'required|regex_match[/^[A-Za-z\s]+$/]',
            'errors' => array(
                'required' => 'El campo %s es obligatorio.',
                'regex_match' => 'El campo %s solo puede contener letras y espacios.'
            )
        )
    ),
    'palabras_clave' => array(
        array(
            'field' => 'palabras_clave',
            'label' => 'Palabras Clave',
            'rules' => 'required|regex_match[/^[A-Za-z\s]+$/]',
            'errors' => array(
                'required' => 'El campo %s es obligatorio.',
                'regex_match' => 'El campo %s solo puede contener letras y espacios.'
            )
        )
    ),
    'fecha_publicacion' => array(
        array(
            'field' => 'fecha_publicacion',
            'label' => 'Fecha de publicación',
            'rules' => 'required',
            'errors' => array(
                'required' => 'El campo %s es obligatorio.'
            )
        )
    ),
    'id_revista' => array(
        array(
            'field' => 'id_revista',
            'label' => 'Revista',
            'rules' => 'required',
            'errors' => array(
                'required' => 'El campo %s es obligatorio.'
            )
        )
    )
);
