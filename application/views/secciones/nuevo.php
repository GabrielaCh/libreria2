<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Sección</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"> <!-- Font Awesome -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
  <!-- Importación de jQuery Validate - Framework de validación -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><section class="pcoded-main-container">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
    form, table {
      width: 50%;
      float: left;
      margin-right: 10px;
    }
    #imagen-adicional {
      width: 40%;
      float: right;
    }
  </style>
</head>
<body>

<h1> <i class="fa-solid fa-city"></i> Nueva Sección</h1>

<form id="formulario" action="<?php echo site_url('secciones/guardarSeccion'); ?>" method="post" enctype="multipart/form-data">
  <label for="nombre"><b>NOMBRE</b></label>
  <input type="text" name="nombre" id="nombre" class="form-control" value="" required placeholder="Ingrese el nombre">
  <span id="errorNombre" class="error"></span>
  <br>
  <label for="categoria"><b>CATEGORIA</b></label>
  <input type="text" name="categoria" id="categoria" class="form-control" value="" required placeholder="Ingrese la categoría">
  <span id="errorCategoria" class="error"></span>
  <br>
  <label for="nombre_revista"><b>Nombre Revista</b></label>
  <select name="nombre_revista" id="nombre_revista" class="form-control" required>
    <option value="">Seleccionar Revista</option>
    <?php foreach($listadoRevistas as $revista): ?>
      <option value="<?php echo $revista->nombre; ?>"><?php echo $revista->nombre; ?></option>
    <?php endforeach; ?>
  </select>
  <br>

  <!-- Botones -->
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('secciones/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
      <br>
    </div>
  </div>
</form>

<img src="https://www.shutterstock.com/image-photo/beautiful-young-woman-buying-books-260nw-2138386495.jpg" alt="Imagen Adicional">

<!-- Script para validar usando jQuery Validate -->
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre: {
        required: true,
        minlength: 3,
        maxlength: 255,
        letras: true // Agregar la regla personalizada para aceptar solo letras
      },
      categoria: {
        required: true,
        minlength: 1,
        maxlength: 255,
        numeros: true // Agregar la regla personalizada para aceptar solo números
      },
      nombre_revista: {
        required: true,
      }
    },
    messages: {
      nombre: {
        required: "Por favor, ingrese el nombre",
        minlength: "El nombre debe tener al menos 3 caracteres",
        maxlength: "El nombre debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      },
      categoria: {
        required: "Por favor, ingrese la categoría",
        minlength: "La categoría debe tener al menos 3 caracteres",
        maxlength: "La categoría debe tener como máximo 255 caracteres",
        numeros: "Este campo solo acepta números"
      },
      nombre_revista: {
        required: "Por favor, seleccione una revista",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "nombre") {
        error.insertAfter("#errorNombre");
      } else if (element.attr("name") == "categoria") {
        error.insertAfter("#errorCategoria");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");

  // Método personalizado para validar solo números
  jQuery.validator.addMethod("numeros", function(value, element) {
    return this.optional(element) || /^[0-9]+$/.test(value);
  }, "Este campo solo acepta números");
});
</script>
</body>
</html>
