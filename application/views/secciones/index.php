<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Secciones</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1> <i class="fa-solid fa-city"></i> SECCIONES</h1>
            <div class="row">
                <div class="col-md-6 text-md-end">
                    <a href="<?php echo site_url('secciones/nuevo'); ?>" class="btn btn-outline-success">
                        <i class="fas fa-plus-circle"></i>
                        Agregar Sección
                    </a>
                </div>
            </div>
            <?php if ($listadoSecciones): ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>CATEGORÍA</th>
                            <th>NOMBRE REVISTA</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoSecciones as $seccion): ?>
                            <tr>
                                <td><?php echo $seccion->id; ?></td>
                                <td><?php echo $seccion->nombre; ?></td>
                                <td><?php echo $seccion->categoria; ?></td>
                                <td><?php echo $seccion->nombre_revista; ?></td>
                                <td>
                                    <a href="<?php echo site_url('secciones/editar/').$seccion->id; ?>" class="btn btn-warning" title="Editar">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                    <a href="<?php echo site_url('secciones/borrar/').$seccion->id; ?>" class="btn btn-danger delete-confirm" data-id="<?php echo $seccion->id; ?>">
                                        Eliminar
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="confirmacion-mensaje">
                    <?php if ($this->session->flashdata('confirmacion')): ?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('confirmacion'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-danger">
                    No se encontraron secciones registradas
                </div>
            <?php endif; ?>
        </div>
        <br>
        <br>
        <br>
        <div class="col-md-6">
          <br><br><br>
          <h1>Almacenamiento:</h1>
          <br>
          <br>
          <img src="https://i.pinimg.com/originals/e1/59/25/e15925c931a81678a3c2e0c0a40db781.gif" alt="Imagen Adicional" class="img-fluid" style="max-width: 40%; height: auto; animation: moveRight 5s infinite linear;">
          </div>
    </div>
</div>

</body>
</html>
