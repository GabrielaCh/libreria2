<style media="screen">
/* Agrega estilos específicos para Toastr */
.toast-success {
  background-color: #5cb85c; /* Cambia el color de fondo para los mensajes de éxito */
  color: #fff; /* Cambia el color del texto para los mensajes de éxito */
}

.toast-info {
  background-color: #5bc0de; /* Color celeste para los mensajes de bienvenida */
  color: #fff; /* Color del texto para los mensajes de bienvenida */
}


.toast-error {
  background-color: #d9534f; /* Cambia el color de fondo para los mensajes de error */
  color: #fff; /* Cambia el color del texto para los mensajes de error */
}

/* Asegúrate de que los estilos de Toastr se apliquen correctamente en dispositivos móviles */
@media (max-width: 767px) {
  .toast {
      width: 100%;
      max-width: 100%;
      margin: 0;
      padding: 10px;
      border-radius: 0;
  }
}

</style>
<?php if ($this->session->flashdata("bienvenida")): ?>
        <script type="text/javascript">
            toastr.info("<?php echo $this->session->flashdata('bienvenida'); ?>");
        </script>
        <?php $this->session->set_flashdata("bienvenida","") ?>
    <?php endif; ?>

      <?php if ($this->session->flashdata("confirmacion")): ?>
       <script type="text/javascript">
       toastr.success("<?php echo $this->session->flashdata('confirmacion'); ?>");
       </script>

      <?php $this->session->set_flashdata("confirmacion","") ?>
      <?php endif; ?>

      <!-- invocamos la funcion para mensajes de error con toastr -->
      <?php if ($this->session->flashdata("error")): ?>
       <script type="text/javascript">
       toastr.error("<?php echo $this->session->flashdata('error'); ?>");
       </script>

      <?php $this->session->set_flashdata("error","") ?>
      <?php endif; ?>

      <style>
      .required{
        color:red;
        background-color: white;
        border-radius:20px;
        font-size:10px;
        padding-left:5px;
        padding-right:5px;
      }

      .error{
        color:red;
        font-weight:bold;
      }


      input.error{
        border:1px solid red;
      }
      </style>
