
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->

        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ form-element ] start -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h5 class="mt-5">Nuevo Articulo</h5>
                                <hr>
                                <form action="<?php echo site_url('articulos/guardarArticulo') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_corresponsable">
                                  <div class="form-group row">
                                  <label for="titulo" class="col-sm-3 col-form-label">Título</label>
                                  <div class="col-sm-9">
                                      <input type="text" value="<?php echo set_value('titulo'); ?>" class="form-control" name="titulo" id="titulo" placeholder="Título">
                                      <?php echo form_error('titulo', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="resumen" class="col-sm-3 col-form-label">Resumen</label>
                                  <div class="col-sm-9">
                                      <input type="text" value="<?php echo set_value('resumen'); ?>" class="form-control" name="resumen" id="resumen" placeholder="Resumen">
                                      <?php echo form_error('resumen', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="palabras_clave" class="col-sm-3 col-form-label">Palabras Clave</label>
                                  <div class="col-sm-9">
                                      <input type="text" value="<?php echo set_value('palabras_clave'); ?>" class="form-control" name="palabras_clave" id="palabras_clave" placeholder="Palabras Clave">
                                      <?php echo form_error('palabras_clave', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  </div>
                                </div>
                                    <div class="form-group row">
                                        <label for="fecha_publicacion" class="col-sm-3 col-form-label">Fecha de publicacion</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" name="fecha_publicacion" id="fecha_publicacion" placeholder="Fecha de Publicacion" >
                                            <?php echo form_error('fecha_publicacion', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="id_revista" class="col-sm-3 col-form-label">Revista:</label>
                                        <div class="col-sm-9">
                                            <select name="id_revista" id="id_revista" class="form-control" >
                                                <option value="">Seleccione la revista</option> <!-- Nueva opción -->
                                                <?php foreach ($nombresRevistas as $revista): ?>
                                                    <option value="<?php echo $revista['id']; ?>"><?php echo $revista['nombre']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <?php echo form_error('id_revista', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                        </div>
                                    </div>



                                    <div class=" row">
                                      <div class="col-md-12 text-center">
                                        <br>
                                        <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
                                        <a href="<?php echo site_url('articulos/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i>Cancelar </a>
                                    </div>
                                    </div>

                                </form>
                            </div>

                        </div>

                        <script>
                            // Example starter JavaScript for disabling form submissions if there are invalid fields
                            (function() {
                                'use strict';
                                window.addEventListener('load', function() {
                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    var forms = document.getElementsByClassName('needs-validation');
                                    // Loop over them and prevent submission
                                    var validation = Array.prototype.filter.call(forms, function(form) {
                                        form.addEventListener('submit', function(event) {
                                            if (form.checkValidity() === false) {
                                                event.preventDefault();
                                                event.stopPropagation();
                                            }
                                            form.classList.add('was-validated');
                                        }, false);
                                    });
                                }, false);
                            })();
                        </script>




                    </div>
                </div>
                <!-- Input group -->

            </div>
            <!-- [ form-element ] end -->
        </div>
        <!-- [ Main Content ] end -->

    </div>
</section>
