<!-- Incluye los archivos de DataTables y sus extensiones -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/datatables.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/DataTables/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/DataTables/jszip.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/DataTables/buttons.html5.min.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/buttons.dataTables.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/DataTables/Spanish.json'); ?>"></script>



<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ stiped-table ] start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">

                        <h5>Articulos</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                          <?php if ($listadoArticulos): ?>
                            <table id="articulosTable" class="table table-striped">
                              <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>TITULO</th>
                                      <th>RESUMEN</th>
                                      <th>PALABRAS CLAVES</th>
                                      <th>FECHA DE PUBLICACION</th>
                                      <th>REVISTA</th>
                                      <th>ACCIONES</th>
                                    </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($listadoArticulos as $articulo): ?>
                                    <tr>
                                      <td><?php echo $articulo->id ?></td>
                                      <td><?php echo $articulo->titulo; ?></td>
                                      <td><?php echo $articulo->resumen; ?></td>
                                      <td><?php echo $articulo->palabras_clave; ?></td>
                                      <td><?php echo $articulo->fecha_publicacion; ?></td>
                                      <td><?php echo $articulo->nombre_revista; ?></td>
                                      <td>
                                        <a href="<?php echo site_url('articulos/editar/').$articulo->id; ?>"
                                             class="btn btn-warning"
                                             title="Editar">
                                          <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="<?php echo site_url('articulos/borrar/').$articulo->id; ?>"
                                             class="btn btn-danger"
                                             title="Eliminar">
                                          <i class="fa fa-trash"></i>
                                        </a>
                                        <button class="btn btn-outline-success text-center" onclick="generarPDF('<?php echo $articulo->id; ?>', '<?php echo $articulo->id; ?>')">PDF</button>


                                      </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            </table>
                          <?php else: ?>
                            <div class="alert alert-danger">
                                No se encontro Autores registrados
                            </div>
                          <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ stiped-table ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/vfs_fonts.js"></script>
<script>
    function formatFecha(fecha) {
        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        var fechaObjeto = new Date(fecha);

        var dia = fechaObjeto.getUTCDate().toString().padStart(2, "0");
        var mes = meses[fechaObjeto.getUTCMonth()];
        var año = fechaObjeto.getUTCFullYear();

        var fechaFormateada = "Tailandia, " + dia + " de " + mes + " del " + año;

        return fechaFormateada;
    }

    function generarPDF(ID_Revista, ID_Publicacion) {
        var fechaDocumento = '';
        var volumen = '';
        var Numero = '';
        var revista = '';
        var idRespuesta;


        <?php foreach ($listadoArticulos as $articulo): ?>
            if (<?php echo $articulo->id ?> == ID_Publicacion) {
                fechaDocumento = '<?php echo $articulo->fecha_publicacion; ?>';
                volumen = '<?php echo $articulo->resumen; ?>';
                Numero = '<?php echo $articulo->titulo; ?>';
                revista = '<?php echo $articulo->nombre_revista; ?>';
                idRespuesta = '<?php echo $articulo->id ?>';
            }
        <?php endforeach; ?>

        var editorial = 'Nueva Generacion';
        var director = 'Rene Quisaguano';
        var firma = 'No Hay';
        var respuesta = 'Aceptado';
        var articulo = '<?php echo $articulo->titulo; ?>';
        var url = '';
        var autoresL = [' Iza Alexander \n Gutierrez Edwin \n Quilumba Marisol'];
        var aceptado = idRespuesta == 1;

        var docDefinition = {
            content: [
                { text: 'REVISTA ' + revista.toLocaleUpperCase(), alignment: 'right', fontSize: 20, color: "#00205b", bold: true },
                { text: formatFecha(fechaDocumento), alignment: 'right', marginTop: 20 },
                { text: autoresL.join('\n'), marginTop: 10, lineHeight: 2, bold: true, listStyleType: 'none' },
                { text: 'Presentes', marginTop: 10, bold: true },
                { text: 'Reciban un cordial saludo y sirva este medio para informarle que, una vez realizado el proceso de arbitraje, el Comité Editorial de la revista ' + revista.toLocaleUpperCase() +' ha decidido PUBLICAR su artículo titulado:',lineHeight: 1.5, marginTop: 10, fontSize: aceptado ? 12 : 0 },
                { text: '"' + articulo + '"', marginTop: 20, bold: true, fontSize: 15, alignment: 'center', lineHeight: 2 },
                { text: 'Mismo que cumple con los lineamientos estipulados para la publicación.', lineHeight: 1.5, marginTop: 10, fontSize: aceptado ? 12 : 0 },
                { text: 'Su artículo es presentado en forma digital y formato PDF que se incluye en el volumen '+volumen+' , número '+Numero+' de nuestra Revista con ISSN: 1026-0994: ', lineHeight: 1.5, marginTop: 10, fontSize: aceptado ? 12 : 0 },
                { text: url, link: url, color: '#0099cc', decoration: 'underline', fontSize: aceptado ? 12 : 0 },

                { text: 'ATENTAMENTE', marginTop: 5, bold: true, alignment: 'center' },

                { text: director, marginTop: 20, alignment: 'center' },
                { text: 'Director Editorial', marginTop: 5, alignment: 'center' },
            ]
        };

        pdfMake.createPdf(docDefinition).open(); // Abrir el PDF en una nueva ventana
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#articulosTable').DataTable({
            language: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf fa-3x"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: 'btn-pdf',
                    action: function (e, dt, button, config) {
                        button.blur();
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, button, config);
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel fa-3x"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'btn-excel',
                    action: function (e, dt, button, config) {
                        button.blur();
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                    }
                }
            ]
        });
    });
</script>
