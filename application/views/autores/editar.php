<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->

        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ form-element ] start -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">


                          <div class="container">
                            <div class="row justify-content-center">
                              <div class="col-md-8">
                                <h1  class="text-center">Editar Autor</h1>
                                <form class="" method="post" action="<?php echo site_url('Autores/actualizarAutor'); ?>" enctype="multipart/form-data">
                                  <input type="hidden" name="id" id="id"
                                	value="<?php echo $autorEditar->id; ?>">
                                  <label for="">
                                    <b>Nombre del Autor:</b>
                                  </label>
                                  <input type="text" name="nombre" id="nombre"
                                	value="<?php echo $autorEditar->nombre; ?>"
                                  placeholder="Ingrese el nombre..." class="form-control" required>
                                  <br>
                                  <label for="">
                                    <b>Apellido:</b>
                                  </label>
                                  <input type="text" name="apellido" id="apellido"
                                	value="<?php echo $autorEditar->apellido; ?>"
                                  placeholder="Ingrese el apellido..." class="form-control" required>
                                  <br>
                                  <label for="">
                                    <b>Nacionalidad:</b>
                                  </label>
                                  <input type="text" name="nacionalidad" id="nacionalidad"
                                	value="<?php echo $autorEditar->nacionalidad; ?>"
                                  placeholder="Ingrese la nacionalidad..." class="form-control" required>
                                  <br>
                                  <label for="">
                                    <b>Fecha de Nacimiento:</b>
                                  </label>
                                  <input type="date" name="fecha_nacimiento" id="fecha_nacimiento"
                                	value="<?php echo $autorEditar->fecha_nacimiento; ?>"
                                  placeholder="Ingrese la fecha de nacimiento..." class="form-control" required>
                                  <br>
                                  <label for="">
                                    <b>Correo:</b>
                                  </label>
                                  <input type="text" name="correo" id="correo"
                                	value="<?php echo $autorEditar->correo; ?>"
                                  placeholder="Ingrese el correo..." class="form-control" required>
                                  <br>

                                  <label for=""><b>INGRESE EL AUTOR:</b></label>
                                      <?php if (!empty($autorEditar->foto)): ?>
                                            <br>
                                            <img src="<?php echo base_url('uploads/autores/' . $autorEditar->foto); ?>" alt="Carnet del Doctor" style="max-width: 200px;">
                                        <?php else: ?>
                                            <p>No se ha adjuntado la revista para este autor.</p>
                                        <?php endif; ?>
                                        <br>
                                        <input type="file" name="foto" id="foto" class="form-control">
                                        <br>

                                  <br>
                                  <br>

                                  <div class="col-md-12 text-center">

                                      <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                      <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

                                  </div>



                                    </div>






                                  <!-- Contenido del formulario -->
                                </form>
                              </div>
                            </div>
