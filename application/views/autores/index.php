<!-- [ Main Content ] start -->

<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ stiped-table ] start -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <div class="col-md-12 text-end">
                        <a href="<?php echo site_url('autores/nuevo'); ?>" class="btn btn-outline-success">
                          <i class="fa fa-plus-circle"></i> Agregar Autores</a>
                        <br> <br>
                      </div>
                        <h5>Autores</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                          <?php if ($listadoAutores): ?>
                            <table class="table table-striped">
                              <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>NOMBRE</th>
                                      <th>APELLIDO</th>
                                      <th>NACIONALIDADO</th>
                                      <th>FECHA DE NACIMIENTO</th>
                                      <th>CORREO</th>
                                      <th>FOTO</th>
                                      <th>ACCIONES</th>
                                    </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($listadoAutores as $autor): ?>
                                    <tr>
                                      <td><?php echo $autor->id ?></td>
                                      <td><?php echo $autor->nombre; ?></td>
                                      <td><?php echo $autor->apellido; ?></td>
                                      <td><?php echo $autor->nacionalidad; ?></td>
                                      <td><?php echo $autor->fecha_nacimiento; ?></td>
                                      <td><?php echo $autor->correo; ?></td>

                                      <td>
                                        <?php if ($autor->foto!=""): ?>
                                         <img src="<?php echo base_url('uploads/autores/').$autor->foto; ?>"
                                         height="100px" alt="">
                                       <?php else: ?>
                                         N/A
                                       <?php endif; ?>
                                      </td>


                                      <td>
                                        <a href="<?php echo site_url('autores/editar/').$autor->id; ?>"
                                             class="btn btn-warning"
                                             title="Editar">
                                          <i class="fa fa-pen"></i>
                                        </a>
                                        <a href="<?php echo site_url('autores/borrar/').$autor->id; ?>"
                                             class="btn btn-danger"
                                             title="Eliminar">
                                          <i class="fa fa-trash"></i>
                                        </a>
                                      </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            </table>
                          <?php else: ?>
                            <div class="alert alert-danger">
                                No se encontro Autores registrados
                            </div>
                          <?php endif; ?>
                        </div>
                    </div>

                </div>

            </div>


            <!-- [ stiped-table ] end -->
            <!-- [ Contextual-table ] start -->

            <!-- [ Contextual-table ] end -->
            <!-- [ Background-Utilities ] start -->

            <!-- [ Background-Utilities ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</section>
