<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->

        <!-- [ breadcrumb ] end -->
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ form-element ] start -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                          <div class="container">
                            <div class="row justify-content-center">
                              <div class="col-md-8">
                                <h1 class="text-center">Nueva Autor</h1>
                                <form class="" action="<?php echo site_url('autores/guardarAutores') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_autor"  >

                                  <label for=""> <b>Nombre del Autor:</b> </label>
                                  <input type="text" value="<?php echo set_value('nombre'); ?>" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre del Autor">
                                   <?php echo form_error('nombre', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>

                                  <label for=""> <b>Apellido:</b> </label>
                                  <input type="text" value="<?php echo set_value('apellido'); ?>" name="apellido" id="apellido"class="form-control" placeholder="Ingrese el apellido  ">
                                  <?php echo form_error('apellido', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>

                                  <label for=""> <b>Nacionalidad:</b> </label>
                                  <input type="text" value="<?php echo set_value('nacionalidad'); ?>" name="nacionalidad" id="nacionalidad"class="form-control" placeholder="Ingrese la nacionalidad ">
                                   <?php echo form_error('nacionalidad', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>

                                  <label for=""> <b>fecha de nacimiento:</b> </label>
                                  <input type="date" value="<?php echo set_value('fecha_nacimiento'); ?>"name="fecha_nacimiento" id="fecha_nacimiento"class="form-control" placeholder="Ingrese la fecha de nacimiento">
                                   <?php echo form_error('fecha_nacimiento', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>

                                  <label for=""> <b>Correo:</b> </label>
                                  <input type="correo" value="<?php echo set_value('correo'); ?>" name="correo" id="correo"class="form-control" placeholder="Ingrese el correo ">
                                   <?php echo form_error('correo', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>

                                  <label for=""><b>Foto:</b></label>
                                  <input type="file" value="<?php echo set_value('foto'); ?>" name="foto" id="foto" class="form-control"  accept="image/*">
                                  <?php echo form_error('foto', '<div class="text-danger">', '</div>'); ?> <!-- Mostrar mensaje de error -->
                                  <br>


                                  <div class="row">

                                    <br>
                                    <div class=" row">
                                      <div class="col-md-12 text-center">
                                        <br>
                                        <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
                                        <a href="<?php echo site_url('autores/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i>Cancelar </a>
                                    </div>
                                  </div>
                                </form>

                            </div>
                          </div>
                          </div>
