
    <h1>Nombres de Autores:</h1>
    <ul>
        <?php foreach ($nombres_autores as $autor): ?>
            <li><?php echo $autor->nombre; ?></li>
        <?php endforeach; ?>
    </ul>

    <h1>Nombres de Revistas:</h1>
    <ul>
        <?php foreach ($nombres_revistas as $revista): ?>
            <li><?php echo $revista->nombre; ?></li>
        <?php endforeach; ?>
    </ul>

    <h1>Fotos de Indexación:</h1>
    <?php foreach ($fotos_indexacion as $indexacion): ?>
        <img src="<?php echo base_url($indexacion->foto); ?>" alt="Foto de Indexación">
    <?php endforeach; ?>
