<h1> <i class="fa-solid fa-city"></i></i>REVISTAS</h1>

<!-- Agregar boton Hospitales -->
<div class="row">
  <div class="col-md-12 text-end">   <!--text-end-> para poner el boton a la derecha-->
    <a href="<?php echo site_url('revistas/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Revista
    </a>

    <br>
  </div>


</div>

<?php if ($listadoRevistas): ?>
  <!--Tabla Estatica-->

    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>FECHA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoRevistas as $revista): ?>
                <tr>
                  <td><?php echo $revista->id; ?></td>
                  <td><?php echo $revista->nombre; ?></td>
                  <td><?php echo $revista->fecha; ?></td>

                  <!--Boton eliminar-->
                  <td>
                    <a href="<?php echo site_url('revistas/editar/').$revista->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                      <a href="<?php echo site_url('revistas/borrar/').$revista->id; ?>" class="btn btn-danger">
                        Eliminar
                      </a>
                  </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<?php else: ?>

  <div class="alert alert-danger">               <!--PAra enviar mensaje de alerta-->
      No se encontraron Revistas registradas
  </div>
<?php endif; ?>
