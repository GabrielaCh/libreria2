<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Revista</title>
  <!-- Agrega los enlaces a las bibliotecas de estilos CSS y fuentes de iconos -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <!-- Agrega el enlace al archivo CSS personalizado -->
  <link rel="stylesheet" href="styles.css">
  <style>
    .error {
      color: red;
      font-size: 12px;
    }
  </style>
</head>
<body>
  <h1><i class="fas fa-city"></i> Nueva Revista</h1>

  <div class="row">
    <div class="col-md-2"></div>
    <form class="col-md-8" action="<?php echo site_url('revistas/guardarRevista'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
      <label for="nombre"><b>NOMBRE</b></label>
      <input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo set_value('nombre'); ?>" placeholder="Ingrese el nombre">
      <span id="errorNombre" class="error"></span><br>

      <label for="fecha"><b>FECHA</b></label>
      <input type="date" name="fecha" id="fecha" class="form-control" value="<?php echo set_value('fecha'); ?>" placeholder="Ingrese una fecha">
      <span id="errorFecha" class="error"></span><br>

      <!-- Botones -->
      <div class="row">
        <div class="col-md-12 text-center">
          <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button>&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url('revistas/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i> CANCELAR</a>
          <br><br><br>
        </div>
      </div>
    </form>
  </div>

<script>
function validarFormulario() {
  var regex = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var nombre = document.getElementById("nombre").value.trim();
  var fecha = document.getElementById("fecha").value.trim();
  var error = false;

  // Validar campo Nombre
  if (nombre === "") {
    document.getElementById("errorNombre").innerHTML = "El campo Nombre es obligatorio.";
    error = true;
  } else if (!regex.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }

  // Validar campo Fecha
  if (fecha === "") {
    document.getElementById("errorFecha").innerHTML = "El campo Fecha de publicación es obligatorio.";
    error = true;
  } else {
    // Validar que la fecha no sea menor al año 2000
    var dateParts = fecha.split("-");
    var year = parseInt(dateParts[0]);
    if (year < 2000) {
      document.getElementById("errorFecha").innerHTML = "La fecha no puede ser menor al año 2000.";
      error = true;
    } else {
      document.getElementById("errorFecha").innerHTML = "";
    }
  }

  // Otros campos pueden ser validados de manera similar

  return !error;
}
</script>

</body>
</html>
