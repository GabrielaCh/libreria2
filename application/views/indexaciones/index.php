<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ stiped-table ] start -->
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="col-md-12 text-end">
                            <a href="<?php echo site_url('indexaciones/nuevo'); ?>" class="btn btn-outline-success">
                                <i class="fa fa-plus-circle"></i> Agregar una Indexacion</a>
                            <br> <br>
                        </div>
                        <h5>Indexaciones</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <?php if ($listadoIndexaciones): ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NOMBRE</th>
                                        <th>DESCRIPCIÓN</th>
                                        <th>ID_REVISTA</th>
                                        <th>FIRMA ELECTRÓNICA</th>
                                        <th>ACCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($listadoIndexaciones as $indexacion): ?>
                                    <tr>
                                        <td><?php echo $indexacion->id ?></td>
                                        <td><?php echo $indexacion->nombre; ?></td>
                                        <td><?php echo $indexacion->descripcion; ?></td>
                                        <td><?php echo $indexacion->id_revista; ?></td>
                                        <td>
                                            <?php if ($indexacion->firma!=""): ?>
                                            <img src="<?php echo base_url('uploads/autores/').$indexacion->firma; ?>"
                                                height="100px" alt="">
                                            <?php else: ?>
                                            N/A
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('indexaciones/editar/').$indexacion->id; ?>"
                                                class="btn btn-warning"
                                                title="Editar">
                                                <i class="fa fa-pen"></i>
                                            </a>
                                            <a href="<?php echo site_url('indexaciones/borrar/').$indexacion->id; ?>"
                                                class="btn btn-danger"
                                                title="Eliminar">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else: ?>
                            <div class="alert alert-danger">
                                No se encontraron Indexaciones registradas
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ stiped-table ] end -->

            <div class="col-md-4">
                <br><br><br>
                <h3>Almacenamiento:</h3>
                <img src="https://media0.giphy.com/media/my4VWVobK6mk/giphy.gif?cid=dc79c3575a940b9e386c706b7775f4e6" alt="Imagen Adicional" class="img-fluid" style="max-width: 70%; height: auto; animation: moveRight 5s infinite linear;">
            </div>

            <!-- [ Contextual-table ] start -->
            <!-- [ Contextual-table ] end -->
            <!-- [ Background-Utilities ] start -->
            <!-- [ Background-Utilities ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</section>
