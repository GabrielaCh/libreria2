<?php
// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener el resto de los datos del formulario
    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $id_revista = $_POST["id_revista"];

    // Verificar si se ha subido un nuevo archivo de firma
    if ($_FILES["firma"]["name"]) {
        // Si se ha subido un nuevo archivo de firma, maneja la carga y actualización de la firma
        // Aquí iría tu lógica para cargar y guardar la nueva firma
        $firma_nueva = $_FILES["firma"]["name"]; // Ejemplo: nombre del archivo de la nueva firma
        // Aquí deberías guardar la nueva firma y actualizar la base de datos con el nuevo nombre
    } else {
        // Si no se ha subido un nuevo archivo de firma, mantener la firma existente
        $firma = $indexacionEditar->firma; // Esto es un ejemplo, asegúrate de obtener el nombre de la firma desde donde corresponda en tu código
    }

    // Continuar con el procesamiento del formulario, incluida la actualización de la base de datos
}
?>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
<!-- Importación de jQuery Validate - Framework de validación -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><section class="pcoded-main-container">
<!-- [ Main Content ] start -->
<section >
    <div class="pcoded-content">
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ form-element ] start -->
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-md-8">
                                        <h1  class="text-center">Editar Indexación</h1>
                                        <form id="formulario" class="needs-validation" method="post" action="<?php echo site_url('Indexaciones/actualizarIndexacion'); ?>" enctype="multipart/form-data">
                                            <input type="hidden" name="id" id="id" value="<?php echo $indexacionEditar->id; ?>">
                                            <label for=""><b>Nombre de indexación</b></label>
                                            <input type="text" name="nombre" id="nombre" value="<?php echo $indexacionEditar->nombre; ?>" placeholder="Ingrese el nombre..." class="form-control" required>
                                            <span id="errorNombre" class="text-danger"></span> <!-- Aquí mostrarás el mensaje de error -->
                                            <br>
                                            <label for=""><b>Descripción:</b></label>
                                            <input type="text" name="descripcion" id="descripcion" value="<?php echo $indexacionEditar->descripcion; ?>" placeholder="Ingrese la descripción..." class="form-control" required>
                                            <span id="errorDescripcion" class="text-danger"></span> <!-- Aquí mostrarás el mensaje de error -->
                                            <br>
                                            <label for="nombre_revista"><b>Id Revista</b></label>
                                            <select name="id_revista" id="id_revista" class="form-control" required>
                                                <option value="">Seleccionar Id Revista</option>
                                                <?php foreach($listadoRevistas as $revista): ?>
                                                    <option value="<?php echo $revista->id; ?>" <?php if ($revista->id == $indexacionEditar->id_revista) echo 'selected'; ?>><?php echo $revista->id; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span id="errorIdRevista" class="text-danger"></span> <!-- Aquí mostrarás el mensaje de error -->
                                            <br>
                                            <label for=""><b>INGRESE LA FIRMA ELECTRÓNICA:</b></label>
                                            <?php if (!empty($indexacionEditar->firma)): ?>
                                                <br>
                                                <img src="<?php echo base_url('uploads/autores/' . $indexacionEditar->firma); ?>" alt="Firma del autor" style="max-width: 200px;">
                                            <?php else: ?>
                                                <p>No se adjuntado ninguna firma.</p>
                                            <?php endif; ?>
                                            <br>
                                            <input type="file" name="firma" id="firma" class="form-control">
                                            <span id="errorFirma" class="text-danger"></span> <!-- Aquí mostrarás el mensaje de error -->
                                            <br>
                                            <div class="col-md-12 text-center">
                                                <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
                                                <a href="<?php echo site_url('indexaciones/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre: {
        required: true,
        minlength: 3,
        maxlength: 255,
        letras: true // Agregar la regla personalizada para aceptar solo letras
      },
      descripcion: {
        required: true,
        minlength: 3,
        maxlength: 255,
      },
      id_revista: {
        required: true,
      }
    },
    messages: {
      nombre: {
        required: "Por favor, ingrese el nombre",
        minlength: "El nombre debe tener al menos 3 caracteres",
        maxlength: "El nombre debe tener como máximo 255 caracteres",
        letras: "Este campo solo acepta letras"
      },
      descripcion: {
        required: "Por favor, ingrese la descripción",
        minlength: "La descripción debe tener al menos 3 caracteres",
        maxlength: "La descripción debe tener como máximo 255 caracteres",
      },
      id_revista: {
        required: "Por favor, seleccione una revista",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "firma") {
        error.insertAfter("#errorFirma");
      } else if (element.attr("name") == "id_revista") {
        error.insertAfter("#errorIdRevista");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Método personalizado para validar solo letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
</script>
