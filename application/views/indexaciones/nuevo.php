<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nueva Indexación</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <!-- Importación de jQuery Validate - Framework de validación -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.3/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body>
<section class="pcoded-main-container">
  <div class="pcoded-content">
    <div class="row">
      <!-- [ form-element ] start -->
      <div class="col-sm-8">
        <div class="card">
          <div class="card-body">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-8">
                  <h1 class="text-center">Nueva Indexación</h1>
                  <form id="formulario" action="<?php echo site_url('indexaciones/guardarIndexaciones'); ?>" method="post" enctype="multipart/form-data">
                    <label for="nombre"><b>NOMBRE y PosFirma</b></label>
                    <input type="text" name="nombre" id="nombre" class="form-control" value="" placeholder="Ingrese el nombre">
                    <span id="errorNombre" class="error"></span><br>
                    <label for="descripcion"><b>DESCRIPCIÓN</b></label>
                    <textarea name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese la descripción"></textarea>
                    <span id="errorDescripcion" class="error"></span><br>
                    <label for="id_revista"><b>ID Revista</b></label>
                    <?php if (isset($listadoRevistas)): ?>
                      <select name="id_revista" id="id_revista" class="form-control">
                        <option value="">Seleccionar Revista</option>
                        <?php foreach($listadoRevistas as $revista): ?>
                          <option value="<?php echo $revista->id; ?>"><?php echo $revista->id; ?></option>
                        <?php endforeach; ?>
                      </select>
                    <?php else: ?>
                      <p>No se encontraron revistas.</p>
                    <?php endif; ?>
                    <span id="errorIdRevista" class="error"></span><br>
                    <label for="firma"><b>FIRMA ELECTRÓNICA:</b></label>
                    <input type="file" name="firma" id="firma" class="form-control" required accept="image/*">
                    <span id="errorFirma" class="error"></span><br>
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
                        <a href="<?php echo site_url('indexaciones/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i>Cancelar</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Columna para la imagen -->
      <div class="col-md-4">
        <br><br><br>
        <h3>Ingrese sus datos:</h3>
        <img src="https://i.pinimg.com/originals/1c/74/9e/1c749e57b0daf3ade2425c982f870367.gif" alt="Imagen Adicional" class="img-fluid" style="max-width: 70%; height: auto; animation: moveRight 5s infinite linear;">
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  // Validación del formulario usando jQuery Validate
  $('#formulario').validate({
    rules: {
      nombre: {
        required: true,
        minlength: 1,
        maxlength: 255,
        letras: true // Aquí se agrega la validación de letras
      },
      descripcion: {
        required: true,
        minlength: 3,
        maxlength: 255,
      },
      id_revista: {
        required: true,
      },
      firma: {
        required: true,
      }
    },
    messages: {
      nombre: {
        required: "Por favor, ingrese el nombre",
        minlength: "El nombre debe tener al menos 3 caracteres",
        maxlength: "El nombre debe tener como máximo 255 caracteres",
      },
      descripcion: {
        required: "Por favor, ingrese la descripción",
        minlength: "La descripción debe tener al menos 3 caracteres",
        maxlength: "La descripción debe tener como máximo 255 caracteres",
      },
      id_revista: {
        required: "Por favor, seleccione una revista",
      },
      firma: {
        required: "Por favor, seleccione una firma electrónica",
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr("name") == "firma") {
        error.insertAfter("#errorFirma");
      } else if (element.attr("name") == "id_revista") {
        error.insertAfter("#errorIdRevista");
      } else {
        error.insertAfter(element);
      }
    }
  });

  // Definición del método de validación de letras
  jQuery.validator.addMethod("letras", function(value, element) {
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);
  }, "Este campo solo acepta letras");
});
</script>
</body>
</html>
