<h1><i class="fa-solid fa-city"></i>EDITAR EDITORIAL</h1>
<div class="row">
	<div class="col-md-2">

	</div>
	<form class="col-md-8" method="post" action="<?php echo site_url('editoriales/actualizarEditorial'); ?>" enctype="multipart/form-data" onsubmit="return validarFormulario()">
		<input type="hidden" name="id" id="id"
		value="<?php echo $editorialEditar->id; ?>">
	  <label for="">
	    <b>Nombre:</b>
	  </label>
	  <input type="text" name="nombre" id="nombre"
		value="<?php echo $editorialEditar->nombre; ?>"
	  placeholder="Ingrese el nombre..." class="form-control" required>
	  <span id="errorNombre" class="error"></span>
	  <br>
	  <label for="">
	    <b>Direccion:</b>
	  </label>
	  <input type="text" name="direccion" id="direccion"
		value="<?php echo $editorialEditar->direccion; ?>"
	  placeholder="Ingrese el nombre..." class="form-control" required>
	  <span id="errorDireccion" class="error"></span>
	  <br>
	  <label for="">
	    <b>Telefono:</b>
	  </label>
	  <input type="text" name="telefono" id="telefono"
		value="<?php echo $editorialEditar->telefono; ?>"
	  placeholder="Ingrese el numero de telefono..." class="form-control" required>
	  <span id="errorTelefono" class="error"></span>
	  <br>
	  <label for="">
	    <b>Correo:</b>
	  </label>
	  <input type="text" name="correo" id="correo"
		value="<?php echo $editorialEditar->correo; ?>"
	  placeholder="Ingrese la editorial..." class="form-control" required>
	  <span id="errorCorreo" class="error"></span>
	  <br>
		<label for="id_revista" class="col-sm-3 col-form-label">Revista:</label>
		<div class="col-sm-9">
		    <select name="id_revista" id="id_revista" class="form-control" required>
		        <option value="">Seleccione la revista</option>
		        <?php foreach ($nombresRevistas as $revista): ?>
		            <option value="<?php echo $revista['id']; ?>" <?php echo ($revista['id'] == $editorialEditar->id_revista) ? 'selected' : ''; ?>><?php echo $revista['nombre']; ?></option>
		        <?php endforeach; ?>
		    </select>
		</div>
		<br>


	    <br>
	    <div class="row">
	      <div class="col-md-12 text-center">
	        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar </button> &nbsp &nbsp
	        <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

	      </div>

	    </div>
	</form>


</div>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>

<script>
function validarFormulario() {
  var regexNombre = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var regexTelefono = /^09[0-9]{8}$/;
  var regexCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  var nombre = document.getElementById("nombre").value;
  var direccion = document.getElementById("direccion").value;
  var telefono = document.getElementById("telefono").value;
  var correo = document.getElementById("correo").value;
	var nombreRevista = document.getElementById("nombre_revista").value;

  var error = false;

  // Validar nombre
  if (!regexNombre.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }

  // Validar dirección
  if (!regexNombre.test(direccion)) {
    document.getElementById("errorDireccion").innerHTML = "Por favor, ingrese solo letras en el campo Dirección.";
    error = true;
  } else {
    document.getElementById("errorDireccion").innerHTML = "";
  }
	if (isNaN(nombreRevista)) {
    document.getElementById("errorNombreRevista").innerHTML = "El nombrede la revista debe ser un número.";
    error = true;
  } else {
    document.getElementById("errorNombreRevista").innerHTML = "";
  }

  // Validar teléfono
  if (!regexTelefono.test(telefono)) {
    document.getElementById("errorTelefono").innerHTML = "Por favor, ingrese un teléfono válido que comience con '09' y tenga 10 dígitos en total.";
    error = true;
  } else {
    document.getElementById("errorTelefono").innerHTML = "";
  }

  // Validar correo
  if (!regexCorreo.test(correo)) {
    document.getElementById("errorCorreo").innerHTML = "Por favor, ingrese un correo electrónico válido.";
    error = true;
  } else {
    document.getElementById("errorCorreo").innerHTML = "";
  }

  return !error;
}
</script>
<br>
<br>
