<h1><i class="fa-solid fa-city"></i> Nueva Editorial</h1>
<br>
<div class="row">
  <div class="col-md-2">

  </div>
  <form class="col-md-8" action="<?php echo site_url('editoriales/guardarEditorial'); ?>" method="post" enctype="multipart/form-data" onsubmit="return validarFormulario()">
    <label for=""><b>NOMBRE</b></label>
    <input type="text" name="nombre" id="nombre" class="form-control" value="" required placeholder="Ingrese el nombre">
    <span id="errorNombre" class="error"></span>
    <br>
    <label for=""><b>DIRECCION</b></label>
    <input type="text" name="direccion" id="direccion" class="form-control" value="" required placeholder="Ingrese la direccion">
    <span id="errorDireccion" class="error"></span>
    <br>
    <label for=""><b>TELEFONO</b></label>
    <input type="text" name="telefono" id="telefono" class="form-control" value="" required placeholder="Ingrese el telefono de la editorial">
    <span id="errorTelefono" class="error"></span>
    <br>
    <label for=""><b>CORREO</b></label>
    <input type="text" name="correo" id="correo" class="form-control" value="" required placeholder="Ingrese el correo">
    <span id="errorCorreo" class="error"></span>
    <br>
    <label for="id_revista" class="col-sm-3 col-form-label">Revista:</label>
    <div class="col-sm-9">
        <select name="id_revista" id="id_revista" class="form-control" required>
            <option value="">Seleccione la revista</option>
            <?php foreach ($nombresRevistas as $revista): ?>
                <option value="<?php echo $revista['id']; ?>"><?php echo $revista['nombre']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <br>
    <br>
    <br>
    <!-- Botones -->
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-check fa-spin"></i>&nbsp;&nbsp; GUARDAR</button> &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('editoriales/index'); ?>" class="btn btn-danger"><i class="fas fa-window-close fa-spin"></i>CANCELAR </a>
        <br>
      </div>
    </div>
  </form>

</div>

<style>
  .error {
    color: red;
    font-size: 12px;
  }
</style>



<script>
function validarFormulario() {
  var regexNombre = /^[A-Za-zÁÉÍÓÚáéíóúÑñ\s]+$/;
  var regexTelefono = /^09[0-9]{8}$/;
  var regexCorreo = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  var nombre = document.getElementById("nombre").value;
  var direccion = document.getElementById("direccion").value;
  var telefono = document.getElementById("telefono").value;
  var correo = document.getElementById("correo").value;
  var revista = document.getElementById("id_revista").value; // Corregido el id aquí

  var error = false;

  // Validar nombre
  if (nombre.trim() === "" || direccion.trim() === "" || telefono.trim() === "" || correo.trim() === "" || revista === "") {
    alert("Por favor, llene todos los campos y seleccione una revista.");
    return false;
  }
  if (!regexNombre.test(nombre)) {
    document.getElementById("errorNombre").innerHTML = "Por favor, ingrese solo letras en el campo Nombre.";
    error = true;
  } else {
    document.getElementById("errorNombre").innerHTML = "";
  }

  // Validar dirección
  if (!regexNombre.test(direccion)) {
    document.getElementById("errorDireccion").innerHTML = "Por favor, ingrese solo letras en el campo Dirección.";
    error = true;
  } else {
    document.getElementById("errorDireccion").innerHTML = "";
  }

  // Validar teléfono
  if (!regexTelefono.test(telefono)) {
    document.getElementById("errorTelefono").innerHTML = "Por favor, ingrese un teléfono válido que comience con '09' y tenga 10 dígitos en total.";
    error = true;
  } else {
    document.getElementById("errorTelefono").innerHTML = "";
  }

  // Validar correo
  if (!regexCorreo.test(correo)) {
    document.getElementById("errorCorreo").innerHTML = "Por favor, ingrese un correo electrónico válido.";
    error = true;
  } else {
    document.getElementById("errorCorreo").innerHTML = "";
  }


  return !error;
}
</script>
