<h1> <i class=""></i></i>EDITORIALES</h1>

<!-- Agregar boton Hospitales -->
<div class="row">
  <div class="col-md-12 text-end">   <!--text-end-> para poner el boton a la derecha-->
    <a href="<?php echo site_url('editoriales/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fas fa-plus-circle"></i>
      Agregar Editorial
    </a>

    <br>
  </div>


</div>

<?php if ($listadoEditoriales): ?>
  <!--Tabla Estatica-->

    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCION</th>
                <th>TELEFONO</th>
                <th>CORREO</th>
                <th>NOMBRE REVISTA</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoEditoriales as $editorial): ?>
                <tr>
                  <td><?php echo $editorial->id; ?></td>
                  <td><?php echo $editorial->nombre; ?></td>
                  <td><?php echo $editorial->direccion; ?></td>
                  <td><?php echo $editorial->telefono; ?></td>
                  <td><?php echo $editorial->correo; ?></td>
                  <td><?php echo $editorial->nombre_revista; ?></td>

                  <!--Boton eliminar-->
                  <td>
                    <a href="<?php echo site_url('editoriales/editar/').$editorial->id; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                      <a href="<?php echo site_url('editoriales/borrar/').$editorial->id; ?>" class="btn btn-danger">
                        Eliminar
                      </a>
                  </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<!--Colocar Modal bootstrap-->


<!--Mensaje si no se encuentra ninguna agencia registrado-->
<?php else: ?>

  <div class="alert alert-danger">               <!--PAra enviar mensaje de alerta-->
      No se encontraron Editoriales registradas
  </div>
<?php endif; ?>
