<?php
//Se crea con el nombre del modelo en plural
//Crear clase Secciones como controlador
  class Secciones extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Seccion");
      $this->load->model("Revista");

    }
    // funcion para renderizar una vista
    public function index(){                 //La funcion index renderiza una vista
      $data["listadoSecciones"]=$this->Seccion->consultarTodos(); //Array asociativo "Data"
      $this->load->view("header");
      $this->load->view("secciones/index",$data);
      $this->load->view("footer");
    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Seccion->eliminar($id);
      $this->session->set_flashdata("confirmacion","Sección eliminada exitosamente");
      redirect("secciones/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo(){
      // Obtener los datos de las revistas
      $data["listadoRevistas"] = $this->Revista->consultarTodos();
      // Cargar la vista pasando los datos
      $this->load->view("header");
      $this->load->view("secciones/nuevo", $data);
      $this->load->view("footer");
    }

    public function guardarSeccion(){

      $datosNuevaSeccion=array(
        "nombre"=>$this->input->post("nombre"),
        "categoria"=>$this->input->post("categoria"),
        "nombre_revista"=>$this->input->post("nombre_revista")
      );
      $this->Seccion->insertar($datosNuevaSeccion);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Sección guardada exitosamente");

      redirect('secciones/index');
    }

      //Renderizar el formulario de edicion
    public function editar($id){
      $data["seccionEditar"]=$this->Seccion->obtenerPorId($id);
      $data["listadoRevistas"] = $this->Revista->consultarTodos();
      $this->load->view("header");
      $this->load->view("secciones/editar",$data);
      $this->load->view("footer");
    }

    public function actualizarSeccion(){
      $id=$this->input->post("id");
      $datosSeccion=array(
        "nombre"=>$this->input->post("nombre"),
        "categoria"=>$this->input->post("categoria"),
        "nombre_revista"=>$this->input->post("nombre_revista")
      );
      $this->Seccion->actualizar($id,$datosSeccion);
      $this->session->set_flashdata("confirmacion",
      "Sección actualizada exitosamente");
      redirect('secciones/index');
    }
  } //Cierre de la clase
?>
