<?php
//Se crea con el nombre del modelo en plural
//Crear clase Hospitales como controlador
  class Editoriales extends CI_Controller
  {

    function __construct()
    {
      // code...
      parent::__construct();
      //Constructor
      //Carga del modelo dentro del controlador
      $this->load->model("Editorial");
      $this->load->model("Revista");

    }
    // funcion para renderizar una vista
    public function index() {
      $data["listadoEditoriales"] = $this->Editorial->consultarTodos();
      // Obtener los nombres de las revistas
      $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();

      $this->load->view("header");
      $this->load->view("editoriales/index", $data); // Pasar $data a la vista
      $this->load->view("footer");

    }

    //Eliminación recibiendo el id por GET
    public function borrar($id){
      $this->Editorial->eliminar($id);
      $this->session->set_flashdata("confirmacion","Editorial eliminada exitosamente");
      redirect("editoriales/index");
    }

    //Renderizacion formulario nuevo
    public function nuevo() {
    // Obtener los nombres de las revistas
      $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();

      // Cargar la vista con los datos de las revistas
      $this->load->view("header");
      $this->load->view("editoriales/nuevo", $data);
      $this->load->view("footer");
    }
     //echo sirve para mostrar datos en pantalla
    //Capturando datos e insertando
    public function guardarEditorial(){

      $datosNuevoEditorial=array(
        "nombre"=>$this->input->post("nombre"),
        "direccion"=>$this->input->post("direccion"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
        "id_revista"=>$this->input->post("id_revista")
      );
      $this->Editorial->insertar($datosNuevoEditorial);
      //flash_data --> crear una session de tipo flash_data
      $this->session->set_flashdata("confirmacion","Editorial guardada exitosamente");

      redirect('editoriales/index');
    }


      //Renderizar el formulario de edicion
      public function editar($id) {
        $data["editorialEditar"] = $this->Editorial->obtenerPorId($id);
        // Obtener los nombres de las revistas
        $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();

        $this->load->view("header");
        $this->load->view("editoriales/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarEditorial(){
      $id=$this->input->post("id");
      $datosEditorial=array(
        "nombre"=>$this->input->post("nombre"),
        "direccion"=>$this->input->post("direccion"),
        "telefono"=>$this->input->post("telefono"),
        "correo"=>$this->input->post("correo"),
        "id_revista"=>$this->input->post("id_revista")
      );
      $this->Editorial->actualizar($id,$datosEditorial);
      $this->session->set_flashdata("confirmacion",
      "Editorial actualizado exitosamente");
      redirect('editoriales/index');
    }


  } //Cierre de la clase


?>
