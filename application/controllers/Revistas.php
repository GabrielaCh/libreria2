<?php
//Se crea con el nombre del modelo en plural
//Crear clase Hospitales como controlador
  class Revistas extends CI_Controller
  {
    function __construct()
    {
        parent::__construct();
        $this->load->model("Revista");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $this->load->view("header");
        $this->load->view("revistas/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Revista->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Revista eliminada exitosamente");
        redirect("revistas/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("revistas/nuevo");
        $this->load->view("footer");
    }

    public function guardarRevista()
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required', array('required' => 'El campo %s es obligatorio.'));
        $this->form_validation->set_rules('fecha', 'Fecha', 'required', array('required' => 'El campo %s es obligatorio.'));

        if ($this->form_validation->run() == FALSE) {
            $this->load->view("header");
            $this->load->view("revistas/nuevo");
            $this->load->view("footer");
        } else {
            $datosNuevoRevista = array(
                "nombre" => $this->input->post("nombre"),
                "fecha" => $this->input->post("fecha"),
            );
            $this->Revista->insertar($datosNuevoRevista);
            $this->session->set_flashdata("confirmacion", "Revista guardada exitosamente");
            redirect('revistas/index');
        }
    }

    public function editar($id)
    {
        $data["revistaEditar"] = $this->Revista->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("revistas/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarRevista()
    {
        $id = $this->input->post("id");

        $this->form_validation->set_rules('nombre', 'Nombre', 'required', array('required' => 'El campo %s es obligatorio.'));
        $this->form_validation->set_rules('fecha', 'Fecha', 'required', array('required' => 'El campo %s es obligatorio.'));

        if ($this->form_validation->run() == FALSE) {
            $data['revistaEditar'] = $this->Revista->obtenerPorId($id);
            $this->load->view("header");
            $this->load->view("revistas/editar", $data);
            $this->load->view("footer");
        } else {
            $datosRevista = array(
                "nombre" => $this->input->post("nombre"),
                "fecha" => $this->input->post("fecha"),
            );
            $this->Revista->actualizar($id, $datosRevista);
            $this->session->set_flashdata("confirmacion", "Revista actualizada exitosamente");
            redirect('revistas/index');
        }
    }
  } //Cierre de la clase


?>
