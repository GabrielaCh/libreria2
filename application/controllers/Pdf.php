<?php


class Pdf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('NombreDelModelo');
    }

    public function index(){

        // Obtener los nombres de autores, revistas y fotos de indexación desde el modelo
        $data['nombres_autores'] = $this->NombreDelModelo->obtenerNombresAutores();
        
        // Cargar la vista
        $this->load->view("header");
        $this->load->view('reportes/index', $data);
        $this->load->view("footer");
    }
}


?>
