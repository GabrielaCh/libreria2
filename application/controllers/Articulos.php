<?php
class Articulos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Articulo");
        $this->load->model("Revista");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["listadoArticulos"] = $this->Articulo->consultarTodos();

        $this->load->view("header");
        $this->load->view("articulos/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Articulo->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Artículo eliminado exitosamente");
        redirect("articulos/index");
    }


    public function nuevo()
    {
        $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();
        $this->load->view("header");
        $this->load->view("articulos/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarArticulo()
    {
      // Cargar el archivo de idioma español para la validación de formularios
      $this->lang->load('form_validation', 'spanish');

      // Establecer reglas de validación
      $this->form_validation->set_rules('titulo', 'Título', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('resumen', 'Resumen', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('palabras_clave', 'Palabras Clave', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('fecha_publicacion', 'Fecha de publicación', 'required');
      $this->form_validation->set_rules('id_revista', 'Revista', 'required');

        if ($this->form_validation->run() == FALSE) {
            // Si la validación falla, cargar nuevamente la vista del formulario con los errores
            $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();
            $this->load->view("header");
            $this->load->view("articulos/nuevo", $data);
            $this->load->view("footer");
        } else {
            // Si la validación es exitosa, guardar el artículo en la base de datos
            $datosNuevoArticulo = array(
                "titulo" => $this->input->post("titulo"),
                "resumen" => $this->input->post("resumen"),
                "palabras_clave" => $this->input->post("palabras_clave"),
                "fecha_publicacion" => $this->input->post("fecha_publicacion"),
                "id_revista" => $this->input->post("id_revista")
            );

            $this->Articulo->insertar($datosNuevoArticulo);
            $this->session->set_flashdata("confirmacion", "Artículo guardado exitosamente");
            redirect('articulos/index');
        }
    }

    public function editar($id)
    {
      $data["articuloEditar"] = $this->Articulo->obtenerPorId($id);
      $data["nombresRevistas"] = $this->Revista->consultarNombresRevistas();
      // Cargar el archivo de idioma español para la validación de formularios
      $this->lang->load('form_validation', 'spanish');

      // Establecer reglas de validación
      $this->form_validation->set_rules('titulo', 'Título', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('resumen', 'Resumen', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('palabras_clave', 'Palabras Clave', 'required|regex_match[/^[A-Za-z\s]+$/]');
      $this->form_validation->set_rules('fecha_publicacion', 'Fecha de publicación', 'required');
      $this->form_validation->set_rules('id_revista', 'Revista', 'required');


      if ($this->form_validation->run() == FALSE) {
        // Si la validación falla, vuelve a cargar la vista de edición con los errores
        $this->load->view("header");
        $this->load->view("articulos/editar", $data);
        $this->load->view("footer");
        } else {
          // Si la validación es exitosa, procede con la actualización del artículo
          $id = $this->input->post("id");
          $datosArticulo = array(
            "titulo" => $this->input->post("titulo"),
            "resumen" => $this->input->post("resumen"),
            "palabras_clave" => $this->input->post("palabras_clave"),
            "fecha_publicacion" => $this->input->post("fecha_publicacion"),
            "id_revista" => $this->input->post("id_revista")
            );
            $this->Articulo->actualizar($id, $datosArticulo);
            $this->session->set_flashdata("confirmacion", "Artículo actualizado exitosamente");
            redirect('articulos/index');
          }
      }

    public function actualizarArticulo()
    {
        $id = $this->input->post("id");
        $this->lang->load('form_validation', 'spanish');

        // Establecer reglas de validación
        $this->form_validation->set_rules('titulo', 'Título', 'required|regex_match[/^[A-Za-z\s]+$/]');
        $this->form_validation->set_rules('resumen', 'Resumen', 'required|regex_match[/^[A-Za-z\s]+$/]');
        $this->form_validation->set_rules('palabras_clave', 'Palabras Clave', 'required|regex_match[/^[A-Za-z\s]+$/]');
        $this->form_validation->set_rules('fecha_publicacion', 'Fecha de publicación', 'required');
        $this->form_validation->set_rules('id_revista', 'Revista', 'required');

        $datosArticulo = array(
          "titulo" => $this->input->post("titulo"),
          "resumen" => $this->input->post("resumen"),
          "palabras_clave"=> $this->input->post("palabras_clave"),
          "fecha_publicacion"=> $this->input->post("fecha_publicacion"),
          "id_revista"=> $this->input->post("id_revista")
        );
        $this->Articulo->actualizar($id, $datosArticulo);
        $this->session->set_flashdata("confirmacion", "Artículo actualizado exitosamente");
        redirect('articulos/index');
    }

}
?>
