<?php
/**
 *
 */
class Autores extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("Autor");
    $this->load->library('form_validation');

  }
  public function index(){
    $data["listadoAutores"]=
    $this->Autor->consultarTodos();

    $this->load->view("header");
    $this->load->view("autores/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Autor->eliminar($id);
    $this->session->set_flashdata("confirmacion","Autor eliminado exitosamnete");
    redirect("autores/index");

  }
  //renderizacion del formulrio de nuevo hospitales
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("autores/nuevo");
    $this->load->view("footer");
  }
  public function guardarAutores(){
    // Cargar el archivo de idioma español para la validación de formularios
    $this->lang->load('form_validation', 'spanish');

    // Establecer reglas de validación
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|regex_match[/^[A-Za-z\s]+$/]');
    $this->form_validation->set_rules('apellido', 'Apellido', 'required|regex_match[/^[A-Za-z\s]+$/]');
    $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required|regex_match[/^[A-Za-z\s]+$/]');

    $this->form_validation->set_rules('correo', 'correo', 'required|valid_email');

  
      if ($this->form_validation->run() == FALSE) {
          // Si la validación falla, cargar nuevamente la vista del formulario con los errores

          $this->load->view("header");
          $this->load->view("autores/nuevo");
          $this->load->view("footer");
      } else {


      /* INICIO PROCESO DE SUBIDA DE ARCHIVO */
            $config['upload_path']=APPPATH.'../uploads/autores/'; //ruta de subida de archivos
            $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
            $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
            $nombre_aleatorio="autor".time()*rand(100,10000);//creando un nombre aleatorio
            $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
            $this->load->library('upload',$config);//cargando la libreria UPLOAD
            if($this->upload->do_upload("foto")){ //intentando subir el archivo
               $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
               $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
            }else{
              $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
            }
     $datosNuevoAutores=array(
      "nombre"=>$this->input->post("nombre"),
      "apellido"=>$this->input->post("apellido"),
      "nacionalidad"=>$this->input->post("nacionalidad"),
      "fecha_nacimiento"=>$this->input->post("fecha_nacimiento"),
      "correo"=>$this->input->post("correo"),
      "foto"=>$nombre_archivo_subido

    );
    $this->Autor->insertar($datosNuevoAutores);
    //flash_dat -<crear una sesion de tipo flash
    $this->session->set_flashdata("confirmacion","Autor guardado exitosamnete");

     redirect('autores/index');
  }}



  //Renderizar el formulario de edicion
    public function editar($id){
      $data["autorEditar"]=$this->Autor->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("autores/editar",$data);
      $this->load->view("footer");
  }




  public function actualizarAutor(){
    $id=$this->input->post("id");

    // Proceso de subida de archivo
    $config['upload_path']=APPPATH.'../uploads/autores/';
    $config['allowed_types']='jpeg|jpg|png';
    $config['max_size']=5*1024;
    $nombre_aleatorio="autor".time()*rand(100,10000);
    $config['file_name']=$nombre_aleatorio;
    $this->load->library('upload',$config);

    $nombre_archivo_subido = "";
    if($this->upload->do_upload("foto")) {
        $dataArchivoSubido = $this->upload->data();
        $nombre_archivo_subido = $dataArchivoSubido["file_name"];
    }

    // Datos del autor
    $datosAutor=array(
        "nombre"=>$this->input->post("nombre"),
        "apellido"=>$this->input->post("apellido"),
        "nacionalidad"=>$this->input->post("nacionalidad"),
        "fecha_nacimiento"=>$this->input->post("fecha_nacimiento"),
        "correo"=>$this->input->post("correo"),
    );

    // Actualizar foto si se cargó un nuevo archivo
    if (!empty($nombre_archivo_subido)) {
        $datosAutor["foto"] = $nombre_archivo_subido;
    }

    // Actualizar autor en la base de datos
    $this->Autor->actualizar($id, $datosAutor);

    // Redireccionar con un mensaje de confirmación
    $this->session->set_flashdata("confirmacion", "Autor actualizado exitosamente");
    redirect('autores/index');
}





}// cierre de la clase
?>
