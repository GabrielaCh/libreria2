<?php
class Indexaciones extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Indexacion");
        $this->load->model("Revista");
    }

    public function index()
    {
        $data["listadoIndexaciones"] = $this->Indexacion->consultarTodos();
        $this->load->view("header");
        $this->load->view("indexaciones/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id)
    {
        $this->Indexacion->eliminar($id);
        $this->session->set_flashdata("confirmacion", "Indexacion eliminada exitosamente");
        redirect("indexaciones/index");
    }

    public function nuevo()
    {
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        if (empty($data["listadoRevistas"])) {
            $data["error"] = "No se encontraron revistas.";
        }
        $this->load->view("header");
        $this->load->view("indexaciones/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarIndexaciones()
    {
        // Proceso de subida de archivo
        $config['upload_path'] = APPPATH . '../uploads/autores/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 5 * 1024;
        $nombre_aleatorio = "indexacion" . time() * rand(100, 10000);
        $config['file_name'] = $nombre_aleatorio;
        $this->load->library('upload', $config);
        if($this->upload->do_upload("firma")) {
            $dataArchivoSubido = $this->upload->data();
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        } else {
            $nombre_archivo_subido = "";
        }

        // Datos para la nueva indexación
        $datosNuevoIndexaciones = array(
            "nombre" => $this->input->post("nombre"),
            "descripcion" => $this->input->post("descripcion"),
            "id_revista" => $this->input->post("id_revista"),
            "firma" => $nombre_archivo_subido
        );

        // Insertar nueva indexación en la base de datos
        $this->Indexacion->insertar($datosNuevoIndexaciones);

        // Crear una sesión de tipo flash
        $this->session->set_flashdata("confirmacion", "Indexación guardada exitosamente");

        redirect('indexaciones/index'); // Redirigir a la página de indexación
    }

    public function editar($id)
    {
        $data["listadoRevistas"] = $this->Revista->consultarTodos();
        $data["indexacionEditar"] = $this->Indexacion->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("indexaciones/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarIndexacion()
    {
        $id = $this->input->post("id");
        $config['upload_path'] = APPPATH . '../uploads/autores/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 5 * 1024;
        $nombre_aleatorio = "indexacion" . time() * rand(100, 10000);
        $config['file_name'] = $nombre_aleatorio;
        $this->load->library('upload', $config);
        $nombre_archivo_subido = "";
        if ($this->upload->do_upload("firma")) {
            $dataArchivoSubido = $this->upload->data();
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        }

        // Datos de la indexación actualizados
        $datosIndexacion = array(
            "nombre" => $this->input->post("nombre"),
            "descripcion" => $this->input->post("descripcion"),
            "id_revista" => $this->input->post("id_revista")
        );

        // Si se subió un nuevo archivo de firma, actualizar la firma en los datos
        if (!empty($nombre_archivo_subido)) {
            $datosIndexacion["firma"] = $nombre_archivo_subido;
        }

        // Actualizar la indexación en la base de datos
        $this->Indexacion->actualizar($id, $datosIndexacion);

        // Crear una sesión de tipo flash
        $this->session->set_flashdata("confirmacion", "Indexación actualizada exitosamente");
        redirect('indexaciones/index');
    }
}
?>
